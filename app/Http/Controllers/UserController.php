<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function showLogin()
    {

        return view('auth.login');
    }

    public function showRegister()
    {
        return view('auth.register');
    }

    public function registerUser(Request $request)
    {
        $errorMsg = [
            'email.email' => 'Bitte eine Korrekte Email angeben!',
            'email.unique' => 'Diese E-Mail ist schon in Verwendung!',
            'password.min' => 'Passwort muss mind. 7 Zeichen lang sein!'
        ];
        $validated = $request->validate([
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'birthdate' => 'required|date',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|max:255|min:7'
        ], $errorMsg);

        $user = new User;
        $user->firstname = $validated['firstname'];
        $user->lastname = $validated['lastname'];
        $user->birthdate = $validated['birthdate'];
        $user->email = $validated['email'];
        $user->password = Hash::make($validated['password']);
        $user->save();

        return redirect()->route('home');

    }

    public function loginUser(Request $request)
    {
        $user = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::attempt($user)) {
            return redirect()->route('home');

        }

    }

    public function logoutUser(){
        session()->flush();
        return redirect()->route('home');
    }
}
