<?php

namespace App\Http\Controllers;
use App\Models\{Category, Article, Image};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class ArticleController extends Controller
{
    public function newArticle()
    {
        $category = Category::all()->sortBy('name');
        return view('articles.new', ['category' => $category]);
    }


    public function showArticle($id)
    {

        $article = Article::find($id);
        return view('articles.single', ['article' => $article]);
    }


    public function saveArticle(Request $request)
    {
        $errorMsg = [
            'name.required' => 'Bitte den Namen ausfüllen!',
            'description.required' => 'Bitte die Beschreibung ausfüllen',
            'quantity.required' => 'Bitte die Stückzahl ausfüllen',
            'price.required' => 'Bitte den Preis ausfüllen',

        ];
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:65535',
            'quantity' => 'required|integer',
            'price' => 'required',
            'img.*' => [
                'file',
                function ($attribute, $value, $fail) {
                    $allowedExtensions = ['jpeg', 'png', 'jpg'];
                    $extension = strtolower($value->getClientOriginalExtension());

                    if (!in_array($extension, $allowedExtensions)) {
                        $fail('Ungültige Datei. Bitte nur JPEG, PNG oder JPG Dateien hochladen.');
                    }
                },
                'max:2048',
            ],
        ], $errorMsg);


        $article = new Article();
        $article->name = $request->name;
        $article->description = $request->description;
        $article->quantity = $request->quantity;
        $article->price = $request->price;
        $article->category_id = $request->category;
        $article->user_id = Auth::user()->id;
        $article->save();


        foreach ($request->file('img') as $image) {
            $imagePath = $image->store('public/images');
            $imageInfo = pathinfo($imagePath);
            $articleImage = new Image();
            $articleImage->path = $imageInfo['basename'];
            $articleImage->article_id = $article->id;
            $articleImage->save();

        }

    }
}
