<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\{User, ShippingMethod, Image};

class Article extends Model
{
    use HasFactory;

    public $timestamps = false;


    protected $fillable = [
        'name',
        'description',
        'quantity',
        'price',
        'category_id',
        'user_id'

    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function shippingMethods()
    {
        return $this->belongsToMany(ShippingMethod::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
