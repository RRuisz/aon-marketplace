@extends('layouts.auth')

@section('content')
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-12">
                <label class="form-check-label" for="firstname">Vorname</label>
                <input type="text" name="firstname" id="firstname" class="form-control" required>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12">
                <label class="form-check-label" for="lastname">Nachname</label>
                <input type="text" name="lastname" id="lastname" class="form-control" required>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12">
                <label for="birthdate" class="form-check-label">Geburtsdatum</label>
                <input type="date" name="birthdate" id="birthdate" class="form-control" required>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12">
                <label for="email" class="form-check-label">E-Mail Adresse</label>
                <input type="email" name="email" id="email" class="form-control" required>
                @if($errors->has('email'))
                    <span class="text-danger">{{$errors->first('email')}}</span>
                @endif
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12">
                <label for="pwd" class="form-check-label">Passwort</label>
                <input type="password" name="password" id="pwd" class="form-control" required>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <input type="submit" value="Abschicken" class="form-control bg-warning">
            </div>
        </div>
    </form>
    <hr>
    <p>Haben Sie bereits ein Konto?  <a href="{{route('login')}}">	&rarr; Anmelden </a></p>

@endsection
