@extends('layouts.auth')

@section('title', 'Anmelden')

@section('content')

    <form method="post">
        @csrf
        <div class="row">
            <div class="col-12">
                <label class="form-check-label" for="email">E-Mail</label>
                <input type="text" name="email" id="email" class="form-control" required>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-12">
                <label class="form-check-label" for="password">Passwort</label>
                <input type="password " name="password" id="password" class="form-control" required>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-12">
                <input type="submit" value="Abschicken" class="form-control bg-warning">
            </div>
        </div>
    </form>
    <hr>
    <p>Haben Sie noch kein ein Konto?  <a href="{{route('register')}}">	&rarr; Konto erstellen </a></p>


@endsection
