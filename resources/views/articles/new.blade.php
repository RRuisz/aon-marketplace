@extends('layouts.main')

@section('title', 'neuen Artikel anlegen')


@section('content')

<div class="d-flex justify-content-center">
    @foreach($errors as $error)
        {{$error}}
    @endforeach
    <div class="card col-5 bg-white mt-5">
        <div class="card-header">

            <h1>Neuen Artikel anlegen</h1>
        </div>
        <div class="card-body">
            <form class="m-3" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row m-1">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" name="name" id="name"  class="form-control">
                </div>
                <div class="row m-1">
                    <label for="description" class="form-label">Beschreibung</label>
                    <textarea name="description" id="description"  class="form-control" rows="5"></textarea>
                </div>
                <div class="row mt-3">
                    <div class="col-6">
                        <label class="form-label" for="quantity">Stück Anzahl</label>
                        <input type="number" class="form-control" name="quantity" id="quantity">
                    </div>
                    <div class="col-6">
                        <label class="form-label" for="price">Preis</label>
                        <input type="number" class="form-control" id="price" name="price" step=".01">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-6">
                        <label class="form-label">Kategorie</label>
                        <select class="form-control" id="category" name="category">
                            @foreach($category as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-6">
                        <label for="img" class="form-label">Vorschaubilder</label>
                        <input type="file" name="img[]" id="img" multiple class="form-control">
                    </div>

                </div>

                <div class="row col-3 m-1 mt-3">
                    <input type="submit" value="Abschicken" class="btn btn-danger">
                </div>
            </form>
        </div>
    </div>
</div>

<script>

    $('document').ready(function() {

    })
</script>
@endsection
