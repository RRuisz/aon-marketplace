<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> AoN - Marketplace | @yield('title')</title>
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body class="bg-secondary">
    <div class="container-fluid">
        <div class="row d-flex align-items-center justify-content-around bg-dark text-danger p-3">
            <div class="col-2 p-0 ps-3">
                <h2><a href="/" class="navbar-brand">AoN - Marketplace</a></h2>
            </div>
            <div class="col-8">
                <div class="row">
                <div class="col-1 p-0">
                    <select class="form-control bg-secondary">
                        @foreach($category as $category)
                            <option value="{{$category->id}}"> {{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-10 p-0">
                    <input type="text" class="form-control border-danger" placeholder="Suche...">
                </div>
                </div>
            </div>
            <div class="col-2 text-center">

            </div>
            <!--TODO: NAV WEITERBAUEN -->
        </div>
    </div>

@yield('content')
</body>
</html>
