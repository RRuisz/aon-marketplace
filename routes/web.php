<?php

use Illuminate\Support\Facades\Route;
use App\Models\{Category};
use App\Http\Controllers\{UserController, ArticleController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $category = Category::all()->sortBy('name');
    return view('home', ['category' => $category]);
})->name('home');

//User
Route::get('/user/login', [UserController::class, 'showLogin'])->name('login');
Route::post('/user/login', [UserController::class, 'loginUser'])->name('login');
Route::get('/user/register', [UserController::class, 'showRegister'])->name('register');
Route::post('/user/register', [UserController::class, 'registerUser'])->name('register');
Route::get('/user/logout', [UserController::class, 'logoutUser'])->name('logout');

//ARTIKEL
Route::get('/article/{id}', [ArticleController::class, 'showArticle'])->name('single')->where('id', '[0-9]+');
Route::get('/article/new', [ArticleController::class, 'newArticle'])->name('newArticle');
Route::post('/article/new', [ArticleController::class, 'saveArticle'])->name('newArticle');
